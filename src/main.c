
#include "main.h"
#include "math.h"


TIM_HandleTypeDef    TimHandle_input;
TIM_HandleTypeDef    TimHandle_output;

/* Timer Input Capture Configuration Structure declaration */
TIM_IC_InitTypeDef       sConfig_input;
TIM_SlaveConfigTypeDef   sSlaveConfig;

//TIM_OC_InitTypeDef sConfig;

/* Captured Value */
__IO uint32_t            uwIC2Value = 0;
/* Duty Cycle Value */
__IO uint32_t            uwDutyCycle = 0;
/* Frequency Value */
__IO uint32_t            uwFrequency = 255;

uint32_t uhPrescalerValue = 0;
int currentPulseWidth = 14;

TIM_OC_InitTypeDef sConfig_output;

/* Private function prototypes -----------------------------------------------*/
void                     SystemClock_Config(void);
static void              Error_Handler(void);
void                     Print_Info(uint8_t *String, uint32_t Size);
void                     Configure_USART(void);
void                     HAL_TIM_PWM_MspInit(TIM_HandleTypeDef *htim);
void                     Start_Pulse(uint32_t pulseWidth);
void                     Print_Number(int number);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void)
{
  HAL_Init();

  /* Configure the system clock to 80 MHz */
  SystemClock_Config();

  while (1)
  {

      Start_Pulse(currentPulseWidth);  /* Normal servo max46, min10
                                          Shutter servo max25, min14-18
                                        */
      if(currentPulseWidth == 26){
        currentPulseWidth = 14;
      }
      currentPulseWidth += 3;
      HAL_Delay(5000);
      
  }
}

void Start_Pulse(uint32_t pulseWidth){
  TimHandle_output.Instance = TIM1;
  uint32_t frequency = 50;
  uint32_t periodValue = 500;

  uhPrescalerValue = (SystemCoreClock / frequency) / periodValue;

  TimHandle_output.Init.Prescaler         = uhPrescalerValue;
  TimHandle_output.Init.Period            = periodValue; 

  TimHandle_output.Init.ClockDivision     = 0;   
  TimHandle_output.Init.CounterMode       = TIM_COUNTERMODE_UP;
  TimHandle_output.Init.RepetitionCounter = 0;
  

  if (HAL_TIM_PWM_Init(&TimHandle_output) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  sConfig_output.OCMode       = TIM_OCMODE_PWM1;
  sConfig_output.OCPolarity   = TIM_OCPOLARITY_HIGH;
  sConfig_output.OCFastMode   = TIM_OCFAST_DISABLE;
  sConfig_output.OCNPolarity  = TIM_OCNPOLARITY_HIGH;
  sConfig_output.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  sConfig_output.OCIdleState  = TIM_OCIDLESTATE_RESET;
  sConfig_output.Pulse = pulseWidth;


  if (HAL_TIM_PWM_ConfigChannel(&TimHandle_output, &sConfig_output, TIM_CHANNEL_4) != HAL_OK)
    {
      Error_Handler();
    }

    if (HAL_TIM_PWM_Start(&TimHandle_output, TIM_CHANNEL_4) != HAL_OK)
    {
      Error_Handler();
    } 

}

static void Error_Handler(void)
{
  while (1)
  {
  }
}


void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};

  /* MSI is enabled after System reset, activate PLL with MSI as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.MSICalibrationValue = RCC_MSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 40;
  RCC_OscInitStruct.PLL.PLLR = 8;  //default = 2
  RCC_OscInitStruct.PLL.PLLP = 7;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  
  if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    /* Initialization Error */
    while(1);
  }
  
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV8;  //default = div1
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;  
  if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    /* Initialization Error */
    while(1);
  }
}


void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef *htim)
{
  GPIO_InitTypeDef   GPIO_InitStruct;
  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* TIMx Peripheral clock enable */
  TIM1_CLK_ENABLE();

  /* Enable GPIO Channels Clock */
  TIM1_CHANNEL_GPIO_PORT();

  /* Common configuration for all channels */
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;

  GPIO_InitStruct.Alternate = TIM1_GPIO_AF_CHANNEL4;
  GPIO_InitStruct.Pin = TIM1_GPIO_PIN_CHANNEL4;
  HAL_GPIO_Init(TIM1_GPIO_PORT_CHANNEL4, &GPIO_InitStruct);
}


void SysTick_Handler(void)
{
  HAL_IncTick();
}

void TIMx_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&TimHandle_input);
}

